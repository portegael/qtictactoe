# QTicTacToe

QTicTacToe is a simple Tic Tac Toe game developped in Qt 5.9.2 and QML.

![Main Game](/screenshots/ttt_screen_main.png)


You can switch modes at the bottom of the screen, to play against AI or against a friend.

You can also choose the symbol you want to play with.

![Symbols](/screenshots/ttt_screen_symbols.png)


Game is over is there is a winner or a draw. A "victory" panel will be displayed, and you can click it to restart a new game. You will notice that the winner's score increased. If you loose against the AI, a game over panel will be displayed.

![Player 1 One](/screenshots/ttt_screen_p1_win.png)

![AI won](/screenshots/ttt_screen_ai_win.png)
