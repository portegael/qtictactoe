#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>
#include <QScreen>
#include <QString>
#include "creferee.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    // Get device type
    QString target;
    int heightScreen = 500;
    int widthScreen  = 400;

#ifdef Q_OS_ANDROID
    target = "android";
    heightScreen = app.primaryScreen()->availableSize().height(); // 600;
    widthScreen  = app.primaryScreen()->availableSize().width();
#elif defined(Q_OS_LINUX) && defined(Q_PROCESSOR_ARM)
    target = "linuxEmbedded";
#elif defined(Q_OS_LINUX) || defined(Q_OS_WIN) || defined(Q_OS_IOS)
    target = QString("desktop");
#endif

    qDebug() << "target is :" << target;

    CReferee *m_referee = new CReferee(&app);
    engine.rootContext()->setContextProperty("RefereeObject", m_referee);
    engine.rootContext()->setContextProperty("targetName", target);
    engine.rootContext()->setContextProperty("heightScreen", heightScreen);
    engine.rootContext()->setContextProperty("widthScreen", widthScreen);

    qmlRegisterUncreatableType<PlayerEnum> ("TicTacToe", 1, 0, "Player", QStringLiteral("Enum!"));

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;



    return app.exec();
}
