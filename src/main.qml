import QtQuick 2.9
import QtQuick.Window 2.1

import TicTacToe 1.0
import "." // QTBUG-34418, singletons require explicit import to load qmldir file
import "./Components"

Window {
    id: root
    visible: true
    minimumHeight: heightScreen
    minimumWidth :  widthScreen
    title: qsTr("Tic-Tac-Toe")
    color: Style.backgroundColor

    property variant clickableSquare   : [false]
    readonly property var boardGameArray : RefereeObject.qmlBoadGameArray


    onBoardGameArrayChanged: {
        //        console.log("boardGameArrayChanged");
        //        console.log("| " + boardGameArray[0] + " | " + boardGameArray[1] + " | " + boardGameArray[2] + " | ");
        //        console.log("| " + boardGameArray[3] + " | " + boardGameArray[4] + " | " + boardGameArray[5] + " | ");
        //        console.log("| " + boardGameArray[6] + " | " + boardGameArray[7] + " | " + boardGameArray[8] + " | ");

        refreshGrid();

    }

    function resetGrid() {
        for (var i = 0; i < boardGameRepeater.count ; ++i) {
            boardGameRepeater.itemAt(i).squareImagesource = "";
            clickableSquare[i] = true;
        }
    }

    function refreshGrid() {
        for (var i = 0; i < boardGameRepeater.count ; ++i) {
            if(boardGameArray[i] === Player.Cross) {
                clickableSquare[i] = false;
                boardGameRepeater.itemAt(i).squareImagesource = iconSelectorPanelId.playerOneIconSource;
            }
            else if(boardGameArray[i]=== Player.Circle){
                clickableSquare[i] = false;
                boardGameRepeater.itemAt(i).squareImagesource = iconSelectorPanelId.playerTwoIconSource;
            }
        }
    }

    Timer {
        id: timer
        repeat: false
        interval: RefereeObject.delayTimerValue

        onTriggered: {
            gameMouseArea.enabled = false
        }
    }

    Component.onCompleted: {
        resetGrid();
    }

    // Score Player 1
    Column {
        id: headerColumn
        anchors.left: gameCenterId.left
        anchors.top : parent.top
        anchors.topMargin: 10
        width : gameCenterId / 3
        height: Style.headerFooterHeight
        spacing: 5

        Image {
            source: iconSelectorPanelId.playerOneIconSource // "qrc:/Images/Player_1_white.png"
            height: parent.height * 0.8
            fillMode: Image.PreserveAspectFit
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Text {
            font.pixelSize: Style.textFontSize
            text: qsTr("Score : " + RefereeObject.scorePlayerOne.toString())
            anchors.horizontalCenter: parent.horizontalCenter
            color : "white"
        }
    }

    // Title
    Item {
        anchors.top: root.top
        width : gameCenterId.width * 0.50
        height: Style.headerFooterHeight
        anchors.horizontalCenter: gameCenterId.horizontalCenter

        Text {
            font.pixelSize: Style.textFontSizeTitle
            text: qsTr("TIC TAC TOE")
            color : "white"
            anchors.verticalCenter: parent.verticalCenter
            anchors.centerIn: parent
        }
    }

    // Score Player 2
    Column {
        anchors.right: gameCenterId.right
        anchors.top : parent.top
        anchors.topMargin: 10
        width : gameCenterId / 3
        height: Style.headerFooterHeight
        spacing: 5

        Image {
            source: iconSelectorPanelId.playerTwoIconSource // "qrc:/Images/Player_2_white.png"
            height: parent.height * 0.8
            fillMode: Image.PreserveAspectFit
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Text {
            font.pixelSize: Style.textFontSize
            text: qsTr("Score : " + RefereeObject.scorePlayerTwo.toString())
            anchors.horizontalCenter: parent.horizontalCenter
            color : "white"
        }
    }

    // Game
    Rectangle {
        id: gameCenterId
        anchors.top: headerColumn.bottom
        anchors.left: parent.left
        anchors.centerIn: parent
        implicitWidth : Style.boardGameWidthHeight
        implicitHeight: Style.boardGameWidthHeight
        color: Style.emptySquareColor

        Grid{
            id: boardGameGrid
            columns: 3
            rows: 3
            anchors.fill : gameCenterId

            Repeater {
                id: boardGameRepeater
                model: 9
                delegate: Square {
                    MouseArea {
                        id: squareMouseArea
                        anchors.fill: parent

                        onClicked: {

                            gameMouseArea.enabled = true
                            timer.start();

                            if(clickableSquare[index]) {
                                RefereeObject.playerPlayed(index)
                                clickableSquare[index] = false;

                                //                                squareImagesource = boardGameArray[index] === Player.Cross ? iconSelectorPanelId.playerOneIconSource : iconSelectorPanelId.playerTwoIconSource;
                            }
                        }
                    }
                    //                    onSquareImagesourceChanged: {
                    //                        console.log("onSquareImagesourceChanged");
                    //                         refreshGrid();
                    //                    }
                }
            }
        }
    }

    // MouseArea
    MouseArea {
        id: gameMouseArea
        anchors.fill: gameCenterId

        enabled: false

        onClicked: {
            console.log("Click catched")
        }
    }

    // Item selection
    Row {
        anchors.right: gameCenterId.right
        anchors.bottom : parent.bottom
        anchors.bottomMargin: 10
        width : gameCenterId / 3
        height: Style.headerFooterHeight * 0.9
        spacing: 50

        Column {
            height: parent.height
            anchors.verticalCenter: parent.verticalCenter

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                color: Style.titleColor
                font.pixelSize: Style.textFontSize

                text: "IA"
            }

            Image {
                id: imageIaId
                source: RefereeObject.isAiActivated ? "qrc:/Images/switch_on.png" : "qrc:/Images/switch_off.png"
                height: parent.height - 20
                fillMode: Image.PreserveAspectFit
                anchors.horizontalCenter: parent.horizontalCenter

                MouseArea {
                    anchors.fill: parent

                    onClicked: {
                        RefereeObject.isAiActivated = !RefereeObject.isAiActivated;
                        RefereeObject.resetGame()
                        root.resetGrid()
                    }
                }
            }
        }

        Image {
            source: "qrc:/Images/icon_selection_128.png"
            height: parent.height - 20
            fillMode: Image.PreserveAspectFit
            anchors.verticalCenter: parent.verticalCenter

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    iconSelectorPanelId.visible = true
                }
            }
        }
    }

    // Winner Panel
    MouseArea {
        id: winnerModal
        width : parent.width
        height : parent.height
        anchors.centerIn: parent

        visible: ((RefereeObject.winner !== Player.Undefined) || RefereeObject.isDrawGame)

        Rectangle {
            anchors.fill: parent
            color: "gray"
            opacity: 0.66
        }

        Rectangle {

            width : parent.width * 0.85
            height: width
            anchors.centerIn: parent
            opacity: 0.75
            color  : Style.backgroundColor

            Column {

                anchors.top: parent.top
                anchors.topMargin: 10
                width: parent.width
                height: parent.height
                spacing : 10
                Text {
                    id: winnerText
                    height: parent.height* 0.2
                    width : parent.width
                    color: "white"
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: Style.textFontSizeTitle


                    text : {
                        if(RefereeObject.winner === Player.One) {
                            return "Player One won";
                        }
                        else if((RefereeObject.winner === Player.Two)) {
                            if(RefereeObject.isAiActivated) {
                                return "You Lose"
                            }
                            else {
                                return "Player Two won"
                            }
                        }
                        if(RefereeObject.isDrawGame) {
                            return "It's a draw !"
                        }

                        return "";
                    }
                }

                Image {
                    height: parent.height *0.70
                    fillMode: Image.PreserveAspectFit
                    anchors.horizontalCenter: parent.horizontalCenter

                    source: {
                        if(RefereeObject.winner === Player.One) {
                            return iconSelectorPanelId.playerOneIconSource; // "qrc:/Images/winner_1.png";
                        }
                        else if((RefereeObject.winner === Player.Two)) {
                            if(RefereeObject.isAiActivated) {
                                return "qrc:Images/game_over.png";
                            }
                            else {
                                return iconSelectorPanelId.playerTwoIconSource; // "qrc:/Images/winner_2.png";
                            }
                        }
                        if(RefereeObject.isDrawGame) {
                            return "qrc:/Images/draw_game.png";
                        }

                        return "";
                    }
                }
            }
        }

        onClicked: {
            RefereeObject.resetGame()
            root.resetGrid()
        }
    }


    IconSelectorPanel {
        id: iconSelectorPanelId
        height: heightScreen // * 0.5
        width: widthScreen // * 0.8

        // Reset game if image changed
        onPlayerOneIconSourceChanged: {
            RefereeObject.resetGame()
            root.resetGrid()
        }
        onPlayerTwoIconSourceChanged: {
            RefereeObject.resetGame()
            root.resetGrid()
        }
    }
}
