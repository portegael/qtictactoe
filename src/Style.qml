// Style.qml
pragma Singleton
import QtQuick 2.0

QtObject {

    readonly property int boardGameWidthHeight  : widthScreen * 0.85
    readonly property int headerFooterHeight    : heightScreen * 0.1

    readonly property int textFontSize          : 16
    readonly property int textFontSizeTitle     : 25

    readonly property string backgroundColor    : "#222222"
    readonly property string emptySquareColor   : "#333333"
    readonly property string filledSquareColor  : "#444444"

    readonly property string titleColor : "white"
}
