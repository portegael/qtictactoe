#include "creferee.h"
#include <qdebug.h>
#include <unistd.h>
#include <QtGlobal>
#include <QCoreApplication>
#include <QTimer>

CReferee::CReferee(QObject *parent) : QObject(parent),
    m_winner(PlayerEnum::Player::Undefined),
    m_currentPlayer(PlayerEnum::Player::One),
    m_scorePlayerOne(0),
    m_scorePlayerTwo(0),
    m_isDrawGame(false),
    m_isAiActivated(false),
    m_delayTimerValue(250)
{
    QVector<int> m_qmlBoadGameArray(9);

    m_timer = new QTimer();
    connect(m_timer, &QTimer::timeout, this, &CReferee::timeoutSlot);

    resetGame();
    emit playerWon();

}

void CReferee::setAiState(const bool newState)
{
    if(newState != m_isAiActivated) {

        m_isAiActivated = newState;

        emit aiStateChanged();
    }
}

void CReferee::playerPlayed(const int index)
{

    m_qmlBoadGameArray[index] = (m_currentPlayer == PlayerEnum::Player::One ? PlayerEnum::TokenType::Cross : PlayerEnum::TokenType::Circle);

    //            qDebug() << "| " << m_qmlBoadGameArray.at(0) << " | " << m_qmlBoadGameArray.at(1) << " | " << m_qmlBoadGameArray.at(2) << " | ";
    //            qDebug() << "| " << m_qmlBoadGameArray.at(3) << " | " << m_qmlBoadGameArray.at(4) << " | " << m_qmlBoadGameArray.at(5) << " | ";
    //            qDebug() << "| " << m_qmlBoadGameArray.at(6) << " | " << m_qmlBoadGameArray.at(7) << " | " << m_qmlBoadGameArray.at(8) << " | ";

    emit qmlBoardGameArrayChanged();

    // checkIfPlayerWon
    if(checkIfPlayerWon(index)) {
        m_currentPlayer == PlayerEnum::Player::One ? m_scorePlayerOne++ : m_scorePlayerTwo++;
        m_winner = m_currentPlayer;
        emit playerWon();
        return;
    }

    // Draw
    if(checkDraw()) {
        m_isDrawGame = true;
        emit isDrawSignal();
        return;
    }


    // If AI is activated, it plays as Player Two
    if(m_isAiActivated && m_currentPlayer == PlayerEnum::Player::One) {

        m_currentPlayer = PlayerEnum::Player::Two;

        // Set timer to display
        m_timer->start(static_cast<int>(m_delayTimerValue));
    }
    else {
        // Switch player
        m_currentPlayer = (m_currentPlayer == PlayerEnum::Player::One ? PlayerEnum::Player::Two : PlayerEnum::Player::One);
    }
}

void CReferee::resetGame(void)
{
    m_currentPlayer = PlayerEnum::Player::One;
    m_winner = PlayerEnum::Player::Undefined;
    m_isDrawGame = false;

    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            m_boardGameArray[i][j] = PlayerEnum::TokenType::Reset;
        }
    }

    for(int i=0; i < 9; i++) {
        m_qmlBoadGameArray.insert(i, PlayerEnum::TokenType::Reset);
    }

    emit playerWon();
    emit qmlBoardGameArrayChanged();
}

void CReferee::timeoutSlot()
{
    m_timer->stop();

    makeIaMove();
}

bool CReferee::checkIfPlayerWon(const int index)
{
    int tokenType = m_currentPlayer == PlayerEnum::Player::One ? PlayerEnum::TokenType::Circle : PlayerEnum::TokenType::Cross;

    if(index < 0 || index > 9) {
        return false;
    }

    insertToMultidimensionalArray(index, tokenType);

    // Check Horizontal
    for(int i = 0; i < 3; i++) {
        if(m_boardGameArray[i][0] == tokenType && m_boardGameArray[i][1] == tokenType && m_boardGameArray[i][2] == tokenType) {
            return true;
        }
    }

    // Check Vertical
    for(int i = 0; i < 3; i++) {
        if(m_boardGameArray[0][i] == tokenType && m_boardGameArray[1][i] == tokenType && m_boardGameArray[2][i] == tokenType) {
            return true;
        }
    }

    // Check diagonals
    if((m_boardGameArray[0][0] == tokenType && m_boardGameArray[1][1] == tokenType && m_boardGameArray[2][2] == tokenType) ||
            (m_boardGameArray[0][2] == tokenType && m_boardGameArray[1][1] == tokenType && m_boardGameArray[2][0] == tokenType)) {
        return true;
    }

    return false;
}

bool CReferee::checkDraw()
{
    for(int i = 0; i < 3; i++) {
        for(int  j = 0; j < 3; j++) {
            if((m_boardGameArray[i][j] != PlayerEnum::TokenType::Cross) && (m_boardGameArray[i][j] != PlayerEnum::TokenType::Circle)) {
                return false;
            }
        }
    }

    return true;
}

void CReferee::insertToMultidimensionalArray(const int index, const int tokenType)
{
    switch(index)
    {
    case 0:
        m_boardGameArray[0][0] = tokenType;
        break;
    case 1:
        m_boardGameArray[1][0] = tokenType;
        break;
    case 2:
        m_boardGameArray[2][0] = tokenType;
        break;
    case 3:
        m_boardGameArray[0][1] = tokenType;
        break;
    case 4:
        m_boardGameArray[1][1] = tokenType;
        break;
    case 5:
        m_boardGameArray[2][1] = tokenType;
        break;
    case 6:
        m_boardGameArray[0][2] = tokenType;
        break;
    case 7:
        m_boardGameArray[1][2] = tokenType;
        break;
    case 8:
        m_boardGameArray[2][2] = tokenType;
        break;
    default:
        break;
    }
}

int CReferee::getIndexSingleArrary(const int row, const int col) {

    if(row == 0 && col == 0) {
        return 0 ;
    }
    else if(row == 1 && col == 0) {
        return 1 ;
    }
    else if(row == 2 && col == 0) {
        return 2 ;
    }
    else if(row == 0 && col == 1) {
        return 3 ;
    }
    else if(row == 1 && col == 1) {
        return 4 ;
    }
    else if(row == 2 && col == 1) {
        return 5 ;
    }
    else if(row == 0 && col == 2) {
        return 6 ;
    }
    else if(row == 1 && col == 2) {
        return 7 ;
    }
    else if(row == 2 && col == 2) {
        return 8 ;
    }

    return -1;
}

void CReferee::insertToSingleArrary(const int row, const int col, const int tokenType) {

    m_qmlBoadGameArray[getIndexSingleArrary(row, col)] = tokenType;
}


void CReferee::makeIaMove()
{
    Move bestMove = findBestMove(m_boardGameArray);


    qDebug() <<"The Optimal Move is ROW: " << bestMove.row << " COL: "  << bestMove.col ;

    int indexToPlay = getIndexSingleArrary(bestMove.row, bestMove.col);
    qDebug() <<"Index : " << indexToPlay << "\n";

    playerPlayed(indexToPlay);

    emit qmlBoardGameArrayChanged();
}




// This function returns true if there are moves
// remaining on the board. It returns false if
// there are no moves left to play.
bool CReferee::isMovesLeft(int board[3][3]) {
    for (int i = 0; i<3; i++)
        for (int j = 0; j<3; j++)
            if (board[i][j] != PlayerEnum::TokenType::Cross && board[i][j] != PlayerEnum::TokenType::Circle )
                return true;
    return false;
}

// This is the evaluation function as discussed
// in the previous article ( http://goo.gl/sJgv68 )
int CReferee::evaluate(int b[3][3])
{
    // Checking for Rows for X or O victory.
    for (int row = 0; row<3; row++)
    {
        if (b[row][0] == b[row][1] &&
                b[row][1] == b[row][2])
        {
            if (b[row][0] == PlayerEnum::TokenType::Cross)
                return +10;
            else if (b[row][0] == PlayerEnum::TokenType::Circle)
                return -10;
        }
    }

    // Checking for Columns for X or O victory.
    for (int col = 0; col<3; col++)
    {
        if (b[0][col] == b[1][col] &&
                b[1][col] == b[2][col])
        {
            if (b[0][col] == PlayerEnum::TokenType::Cross)
                return +10;

            else if (b[0][col] == PlayerEnum::TokenType::Circle)
                return -10;
        }
    }

    // Checking for Diagonals for X or O victory.
    if (b[0][0] == b[1][1] && b[1][1] == b[2][2])
    {
        if (b[0][0] == PlayerEnum::TokenType::Cross)
            return +10;
        else if (b[0][0] == PlayerEnum::TokenType::Circle)
            return -10;
    }

    if (b[0][2]==b[1][1] && b[1][1]==b[2][0])
    {
        if (b[0][2] == PlayerEnum::TokenType::Cross)
            return +10;
        else if (b[0][2] == PlayerEnum::TokenType::Circle)
            return -10;
    }

    // Else if none of them have won then return 0
    return 0;
}

// This is the minimax function. It considers all
// the possible ways the game can go and returns
// the value of the board
int CReferee::minimax(int board[3][3], int depth, bool isMax)
{
    int score = evaluate(board);

    // If Maximizer has won the game return his/her
    // evaluated score
    if (score == 10)
        return score;

    // If Minimizer has won the game return his/her
    // evaluated score
    if (score == -10)
        return score;

    // If there are no more moves and no winner then
    // it is a tie
    if (isMovesLeft(board)==false)
        return 0;

    // If this maximizer's move
    if (isMax)
    {
        int best = -1000;

        // Traverse all cells
        for (int i = 0; i<3; i++)
        {
            for (int j = 0; j<3; j++)
            {
                // Check if cell is empty
                if ((board[i][j] != PlayerEnum::TokenType::Cross) && (board[i][j] != PlayerEnum::TokenType::Circle) ) {
                    // Make the move
                    board[i][j] = PlayerEnum::TokenType::Cross;

                    // Call minimax recursively and choose
                    // the maximum value
                    best = qMax( best,
                                 minimax(board, depth+1, !isMax) );

                    // Undo the move
                    board[i][j] = PlayerEnum::TokenType::Reset;
                }
            }
        }
        return best;
    }

    // If this minimizer's move
    else
    {
        int best = 1000;

        // Traverse all cells
        for (int i = 0; i<3; i++)
        {
            for (int j = 0; j<3; j++)
            {
                // Check if cell is empty
                if (board[i][j] != PlayerEnum::TokenType::Cross && board[i][j] != PlayerEnum::TokenType::Circle ) {
                    // Make the move
                    board[i][j] =  PlayerEnum::TokenType::Circle;

                    // Call minimax recursively and choose
                    // the minimum value
                    best = qMin(best,
                                minimax(board, depth+1, !isMax));

                    // Undo the move
                    board[i][j] = PlayerEnum::TokenType::Reset;
                }
            }
        }
        return best;
    }
}

// This will return the best possible move for the player
Move CReferee::findBestMove(int board[3][3])
{
    int bestVal = -1000;
    Move bestMove;
    bestMove.row = -1;
    bestMove.col = -1;

    // Traverse all cells, evaluate minimax function for
    // all empty cells. And return the cell with optimal
    // value.
    for (int i = 0; i<3; i++)
    {
        for (int j = 0; j<3; j++)
        {
            // Check if cell is empty
            if (board[i][j] != PlayerEnum::TokenType::Cross && board[i][j] != PlayerEnum::TokenType::Circle ) {
                {
                    // Make the move
                    board[i][j] = PlayerEnum::TokenType::Cross;

                    // compute evaluation function for this
                    // move.
                    int moveVal = minimax(board, 0, false);

                    // Undo the move
                    board[i][j] = PlayerEnum::TokenType::Reset;

                    // If the value of the current move is
                    // more than the best value, then update
                    // best/
                    if (moveVal > bestVal)
                    {
                        bestMove.row = i;
                        bestMove.col = j;
                        bestVal = moveVal;
                    }
                }
            }
        }
    }

    qDebug() <<"The value of the best Move is : " << bestVal;

    return bestMove;
}
