import QtQuick 2.0
import "../"

FocusScope {

    id: base
    anchors.fill : parent
    visible : false
    property string playerOneIconSource : "qrc:/Images/cross_02.png"
    property string playerTwoIconSource : "qrc:/Images/circle_02.png"


    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.8

        // MouseArea to catch mouse events and hide panel
        MouseArea {
            anchors.fill: parent

            onClicked: {
                base.visible = false
            }
        }
    }

    Rectangle {

        width: parent.width * 0.75 // Style.boardGameWidthHeight
        height: parent.height * 0.5 // width
        anchors.right  : parent.right
        anchors.bottom : parent.bottom
        anchors.bottomMargin: 10
        anchors.rightMargin: anchors.bottomMargin



        ListModel {
            id: listModelCrossId
            ListElement { imageSource : "qrc:/Images/cross.png" }
            ListElement { imageSource : "qrc:/Images/cross_02.png" }
            ListElement { imageSource : "qrc:/Images/cross_03.png" }
        }

        ListModel {
            id: listModelCircleId
            ListElement { imageSource : "qrc:/Images/circle.png" }
            ListElement { imageSource : "qrc:/Images/circle_01.png" }
            ListElement { imageSource : "qrc:/Images/circle_02.png" }
        }

        Rectangle  {
            id: backgroundRectangleId
            anchors.fill: parent

            color: Style.emptySquareColor
            border.color: "white"

            Text {
                id: title
                width: parent.width
                anchors.top: parent.top
                anchors.topMargin: 5
                anchors.horizontalCenter: parent.horizontalCenter


                font.pixelSize: Style.textFontSizeTitle
                color: Style.titleColor
                text: "Choose your symbol"
                horizontalAlignment: Text.AlignHCenter


            }

            Row {
                id: rowId
                anchors.top: title.bottom
                anchors.bottom: parent.bottom
                anchors.left: title.left
                width: parent.width
                //                height: parent.height - title.height - 50 // to be fixed
                anchors.topMargin: 5
                anchors.bottomMargin: 5
                anchors.leftMargin: 10
                anchors.rightMargin: anchors.leftMargin

                anchors.horizontalCenter: parent.horizontalCenter

                // Cross symbol panel
                ListView {
                    id: listViewId
                    width: parent.width * 0.4
                    height: parent.height

                    orientation: Qt.Vertical
                    model: listModelCrossId
                    delegate:
                        Rectangle { width : height; height: listViewId.height * 0.3;
                        border.color: Style.backgroundColor;
                        border.width: mouseAreaCrossId.containsMouse ? 2 : 1;
                        anchors.horizontalCenter: parent.horizontalCenter
                        color : {
                            if(base.playerOneIconSource === imageSource || mouseAreaCrossId.containsMouse ) {
                                Style.filledSquareColor
                            }
                            else {
                                backgroundRectangleId.color
                            }
                        }

                        opacity: mouseAreaCrossId.containsMouse ? 1 : 0.7
                        Image { anchors.fill : parent; anchors.margins: 10; source : imageSource}

                        MouseArea {
                            id: mouseAreaCrossId
                            anchors.fill: parent
                            hoverEnabled: true

                            onClicked: {
                                base.playerOneIconSource = imageSource;
                            }
                        }
                    }
                }

                // Separator
                Rectangle {
                    width: parent.width * 0.2
                    height: parent.height * 0.95
                    color: Style.emptySquareColor

                    Rectangle{
                        anchors.centerIn: parent
                        height: parent.height
                        width: 1

                        color: Style.backgroundColor
                    }
                }

                // Circle symbol panel
                ListView {
                    width: parent.width * 0.4
                    height: parent.height
                    orientation: Qt.Vertical
                    model: listModelCircleId
                    delegate:
                        Rectangle { width : height; height: listViewId.height * 0.3;
                        border.color: Style.backgroundColor;
                        border.width: mouseAreaCircleId.containsMouse ? 2 : 1;
                        anchors.horizontalCenter: parent.horizontalCenter
                        color : if(base.playerTwoIconSource === imageSource || mouseAreaCircleId.containsMouse ) {
                                    Style.filledSquareColor
                                }
                                else {
                                    backgroundRectangleId.color
                                }
                        opacity: mouseAreaCircleId.containsMouse ? 1 : 0.7
                        Image { anchors.fill : parent; anchors.margins: 10; source : imageSource}

                        MouseArea {
                            id: mouseAreaCircleId
                            anchors.fill: parent
                            hoverEnabled: true

                            onClicked: {
                                base.playerTwoIconSource = imageSource;
                            }
                        }
                    }
                }
            }
        }
    }
}
