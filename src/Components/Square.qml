import QtQuick 2.9
import TicTacToe 1.0
import "../"

FocusScope {

    id: base
    implicitHeight: parent.height / 3
    implicitWidth : parent.width / 3

    property string squareImagesource : "";

    Rectangle {
        id: rectangleSquare

        anchors.fill: parent
        color : squareImagesource === "" ? Style.emptySquareColor : Style.filledSquareColor
        border.color: Style.backgroundColor

        Image {
            id: squareImage
            anchors.fill: parent
            anchors.margins: 10

            source: base.squareImagesource
        }
    }

    MouseArea {
        anchors.fill: parent

        onPressed: rectangleSquare.opacity = 0.66
    }
}
