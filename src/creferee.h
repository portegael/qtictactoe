#ifndef CREFEREE_H
#define CREFEREE_H

#include <QObject>
#include <QVector>
#include <QTimer>

//_________________________________________________________________________________________________________
/**
 * @brief Enums
 */
class PlayerEnum {
    Q_GADGET

public:

    enum Player { Undefined, One, Two };
    Q_ENUM(Player)

    enum TokenType {Reset, Cross, Circle};
    Q_ENUM(TokenType)

private:
    explicit PlayerEnum();

};
typedef PlayerEnum::Player Player;
typedef PlayerEnum::TokenType Token;

struct Move
{
    int row, col;
};

//_________________________________________________________________________________________________________
/**
 * @brief Class Referee
 */
class CReferee : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVector<int> qmlBoadGameArray        READ getQmlBoardGameArray                   NOTIFY qmlBoardGameArrayChanged )
    Q_PROPERTY(PlayerEnum::Player winner            READ getWinner                              NOTIFY playerWon        )
    Q_PROPERTY(PlayerEnum::Player currentPlayer     READ getCurrentPlayer                                               )
    Q_PROPERTY(uint scorePlayerOne                  READ getScorePlayerOne                      NOTIFY playerWon        )
    Q_PROPERTY(uint scorePlayerTwo                  READ getScorePlayerTwo                      NOTIFY playerWon        )
    Q_PROPERTY(bool isDrawGame                      READ isDrawGame                             NOTIFY isDrawSignal     )
    Q_PROPERTY(bool isAiActivated                   READ isAiActivated      WRITE setAiState    NOTIFY aiStateChanged   )
    Q_PROPERTY(uint delayTimerValue                 READ delayTimerValue                        NOTIFY delayTimerValueChanged   )

public:

    explicit CReferee(QObject *parent = nullptr);

    QVector<int> getQmlBoardGameArray(void)     const { return m_qmlBoadGameArray; }
    PlayerEnum::Player getWinner(void)        const { return m_winner        ; }
    PlayerEnum::Player getCurrentPlayer(void) const { return m_currentPlayer ; }
    uint getScorePlayerOne(void) const              { return m_scorePlayerOne; }
    uint getScorePlayerTwo(void) const              { return m_scorePlayerTwo; }
    bool isDrawGame(void) const                     { return m_isDrawGame    ; }
    bool isAiActivated(void) const                  { return m_isAiActivated ; }
    void setAiState(const bool newState);
    uint delayTimerValue(void) const                { return m_delayTimerValue; }
    Q_INVOKABLE void playerPlayed(const int index);
    Q_INVOKABLE void resetGame(void);

signals:
    void qmlBoardGameArrayChanged();
    void playerWon();
    void isDrawSignal();

    void aiStateChanged();
    void iaPlayed();
    void delayTimerValueChanged();

private slots:
    void timeoutSlot();

private:

    PlayerEnum::Player m_winner;
    PlayerEnum::Player m_currentPlayer;

    uint m_scorePlayerOne;
    uint m_scorePlayerTwo;

    bool m_isDrawGame;
    bool m_isAiActivated;

    int m_boardGameArray[3][3] {{0}};
    QVector<int> m_qmlBoadGameArray;

    bool checkIfPlayerWon(const int index);
    bool checkDraw(void);
    void insertToMultidimensionalArray(const int index, const int tokenType);
    void insertToSingleArrary(const int row, const int col, const int tokenType);
    int getIndexSingleArrary(const int row, const int col);
    void makeIaMove(void);

    bool isMovesLeft(int board[3][3]);
    int evaluate(int b[3][3]);
    int minimax(int board[3][3], int depth, bool isMax);
    Move findBestMove(int board[3][3]);

    QTimer *m_timer;
    uint m_delayTimerValue;

};

#endif // CREFEREE_H
